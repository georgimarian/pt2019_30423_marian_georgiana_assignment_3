package model;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Generates table header using reflection
 * 
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 *
 */
public class Reflection {

	/**
	 * Returns a list of the object attributes' values
	 * 
	 * @param object
	 * @return
	 */
	public static List<String> retrieveValues(Object object) {
		List<String> values = new ArrayList<String>();
		for (Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true); // set modifier to public
			Object value;
			try {
				value = field.get(object);
				values.add(value.toString());
				// System.out.println(field.getName() + "=" + value);

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

		}
		return values;
	}

	/**
	 * Returns a String array of the object's attributes
	 * 
	 * @param object
	 * @return
	 */
	public static List<String> tableHeader(Object object) {
		int i = 0;
		int n = object.getClass().getDeclaredFields().length;
		List<String> str = new ArrayList<String>();
		for (Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			try {
				str.add(field.getName());
				i++;

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		return str;
	}
}

package model;

/**
 * Class corresponding to 'product' table in the database
 * 
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 */
public class Product {

	private int productID;
	private String productName;
	private int stock;
	private int pricePerUnit;

	/**
	 * Explicit product constructor
	 * 
	 * @param productID   represents the product identifier
	 * @param name        represents the product name
	 * @param stock 	  represents the product stock
	 * @param ppu         represents the price per unit
	 */
	public Product(int productID, String name, int stock, int ppu) {
		this.productID = productID;
		this.productName = name;
		this.stock = stock;
		this.pricePerUnit = ppu;
	}

	/**
	 * Explicit constructor lacking the identifier
	 * 
	 * @param name
	 * @param stock
	 * @param ppu
	 */
	public Product(String name, int stock, int ppu) {
		super();
		this.productName = name;
		this.stock = stock;
		this.pricePerUnit = ppu;
	}

	/**
	 * Implicit constructor
	 */
	public Product() {

	}

	/**
	 * @param productID Sets the product identifier
	 */
	public void setProductID(int productID) {
		this.productID = productID;
	}

	/**
	 * @return Returns the product identifier
	 */
	public int getProductID() {
		return productID;
	}

	/**
	 * @return Returns the product name
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName Sets the product name
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return Returns the product stock
	 */
	public int getStock() {
		return stock;
	}

	/**
	 * @param stock Sets the product stock
	 */
	public void setStock(int stock) {
		this.stock = stock;
	}

	/**
	 * @return Returns the product price
	 */
	public int getPricePerUnit() {
		return pricePerUnit;
	}

	/**
	 * @param pricePerUnit Sets the product price
	 */
	public void setPricePerUnit(int pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}

	/**
	 * Returns product in readable manner
	 */
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" " + this.productName);
		stringBuilder.append(" price: " + this.pricePerUnit);
		return stringBuilder.toString();
	}

}

package model;

import dao.ClientDao;
import dao.OrderHasProductDao;

/**
 * Class corresponding to the 'order' table in the database
 * 
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 */
public class Order {

	private int orderID;
	private int totalPrice;
	private int client_clientID;

	/**
	 * Explicit order constructor
	 * 
	 * @param total    represents total price per order
	 * @param clientID represents the identifier of the ordering client
	 */
	public Order(int total, int clientID) {
		this.totalPrice = total;
		this.client_clientID = clientID;
	}

	/**
	 * Explicit constructor using also order identifier
	 * 
	 * @param ID
	 * @param total
	 * @param clientID
	 */
	public Order(int ID, int total, int clientID) {
		this.orderID = ID;
		this.totalPrice = total;
		this.client_clientID = clientID;
	}

	/**
	 * @return Returns order identifier
	 */
	public int getOrderID() {
		return orderID;
	}

	/**
	 * @param orderID Sets order identifier
	 */
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	/**
	 * @return Returns total price
	 */
	public int getTotal() {
		return totalPrice;
	}

	/**
	 * @param total Sets total price
	 */
	public void setTotal(int total) {
		this.totalPrice = total;
	}

	/**
	 * @return Returns client identifier
	 */
	public int getClientID() {
		return client_clientID;
	}

	/**
	 * @param client_clientID sets client identifier
	 */
	public void setClientID(int client_clientID) {
		this.client_clientID = client_clientID;
	}

	/**
	 * Returns order in readable manner
	 */
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Order summary: \n" + ClientDao.selectOne(this.client_clientID).toString()
				+ "\n has paid total price ");
		stringBuilder.append(this.totalPrice + " on \n");
		for (OrderHasProduct o : OrderHasProductDao.findById(this.orderID)) {
			stringBuilder.append(o.toString() + "\n");
		}
		return stringBuilder.toString();
	}
}

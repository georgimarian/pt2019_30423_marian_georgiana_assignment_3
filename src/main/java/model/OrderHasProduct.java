package model;

import dao.ProductDao;

/**
 * Class illustrating the 'many-to-many' relationship between Order and Product
 * 
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 */
public class OrderHasProduct {
	private int order_orderID;
	private int product_productID;
	private int quantity;
	private int price;

	/**
	 * . Explicitly constructs an order_has_product relationship
	 * 
	 * @param order_orderID     the identifier of the corresponding order
	 * @param product_productID the identifier of the corresponding product
	 * @param qty               the ordered quantity
	 * @param price             the price for the ordered quantity
	 */
	public OrderHasProduct(int order_orderID, int product_productID, int qty, int price) {
		this.order_orderID = order_orderID;
		this.product_productID = product_productID;
		this.quantity = qty;
		this.price = price;
	}

	/**
	 * @return Returns order identifier
	 */
	public int getOrder_orderID() {
		return order_orderID;
	}

	/**
	 * @param order_orderID Sets the order identifier
	 */
	public void setOrder_orderID(int order_orderID) {
		this.order_orderID = order_orderID;
	}

	/**
	 * @return Returns product identifier
	 */
	public int getProduct_productID() {
		return product_productID;
	}

	/**
	 * @param product_productID Sets product identifier
	 */
	public void setProduct_productID(int product_productID) {
		this.product_productID = product_productID;
	}

	/**
	 * @return Returns ordered quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity Sets ordered quantity
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return Returns price per ordered quantity
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param price Sets price per ordered quantity
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * Returns relationship content in readable manner
	 */
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("\t" + this.quantity + "x" + ProductDao.findById(product_productID).toString()
				+ " ....... " + this.price + " lei");
		return stringBuilder.toString();
	}
}

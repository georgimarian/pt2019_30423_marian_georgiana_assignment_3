package model;

/**
 * Class corresponding to the 'client' table in the database
 * 
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 */
public class Client {

	private int clientID;
	private String name;
	private String surname;
	private String address;
	private String phone;

	/**
	 * Explicit client constructor
	 * 
	 * @param name    the client's name
	 * @param surname the client's surname
	 * @param addr    the client's address
	 * @param phone   the client's phone number
	 */
	public Client(String name, String surname, String addr, String phone) {
		this.name = name;
		this.surname = surname;
		this.address = addr;
		this.phone = phone;
	}

	/**
	 * Explicit constructor using also client identifier
	 * 
	 * @param ID
	 * @param name
	 * @param surname
	 * @param addr
	 * @param phone
	 */
	public Client(int ID, String name, String surname, String addr, String phone) {
		this.clientID = ID;
		this.name = name;
		this.surname = surname;
		this.address = addr;
		this.phone = phone;
	}

	/**
	 * Implicit constructor
	 */
	public Client() {

	}

	/**
	 * @return Returns client identifier
	 */
	public int getClientID() {
		return clientID;
	}

	/**
	 * @param clientID Sets client identifier
	 */
	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	/**
	 * @return Returns client name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name Sets client name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return Returns client surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname Sets client surname
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return Returns client address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address Sets client address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return returns client phone number
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone Sets client phone number
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Returns client in readable manner
	 */
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" name: " + this.name);
		stringBuilder.append(" " + this.surname);
		stringBuilder.append(" address: " + this.address);
		stringBuilder.append(" phone: " + this.phone);

		return stringBuilder.toString();
	}

}

package presentation;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dao.ClientDao;
import dao.ProductDao;
import model.Client;
import model.Product;
import model.Reflection;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.Font;
import java.awt.Color;
import javax.swing.UIManager;

/**
 * Class implementing the graphical user interface
 * 
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 *
 */
public class View extends JFrame {
	private JTable table;
	private JTable table2;

	private JComboBox comboBox;
	private JComboBox comboBox2;

	private JTextField nameTextField;
	private JTextField surnameTextField;
	private JTextField addressTextField;
	private JTextField phoneTextField;
	private JTextField productNameTextField;
	private JTextField productPPUTextField;
	private JTextField stockTextField;
	private JTextField deleteProductID;
	private JTextField deleteClientId;
	private JTextField orderQuantity;
	private JTextField orderTextField;

	private JButton btnAddProduct;
	private JButton btnUpdateProduct;
	private JButton btnDeleteProduct;
	private JButton btnAddClient;
	private JButton btnUpdateClient;
	private JButton btnDeleteClient;
	private JButton btnAddToOrder;
	private JButton btnDelete;
	private JButton btnComplete;

	public View() {
		setBackground(UIManager.getColor("FormattedTextField.selectionBackground"));
		getContentPane().setBackground(UIManager.getColor("InternalFrame.activeTitleGradient"));
		setTitle("Warehouse Management System");
		getContentPane().setLayout(null);
		this.setBounds(100, 100, 850, 629);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(8, 42, 400, 112);
		getContentPane().add(scrollPane);

		table = new JTable();
		List<Product> products = ProductDao.selectAll();
		scrollPane.setViewportView(table);
		showTable(table, products);

		JScrollPane scrollPane2 = new JScrollPane();
		scrollPane2.setBounds(420, 42, 400, 112);
		getContentPane().add(scrollPane2);

		table2 = new JTable();
		List<Client> clients = ClientDao.selectAll();
		scrollPane2.setViewportView(table2);
		showTable(table2, clients);

		comboBox = new JComboBox<Client>();
		for (Client c : ClientDao.selectAll()) {
			comboBox.addItem(c);
		}
		comboBox.setBounds(12, 421, 400, 31);
		getContentPane().add(comboBox);

		comboBox2 = new JComboBox<Product>();
		for (Product c : ProductDao.selectAll()) {
			comboBox2.addItem(c);
		}
		comboBox2.setBounds(420, 421, 219, 31);
		getContentPane().add(comboBox2);

		btnAddProduct = new JButton("Add product");
		btnAddProduct.setBackground(new Color(30, 144, 255));
		btnAddProduct.setBounds(396, 337, 114, 25);
		getContentPane().add(btnAddProduct);

		btnUpdateProduct = new JButton("Update product");
		btnUpdateProduct.setBounds(531, 337, 138, 25);
		getContentPane().add(btnUpdateProduct);

		btnDeleteProduct = new JButton("Delete product");
		btnDeleteProduct.setBackground(new Color(255, 0, 0));
		btnDeleteProduct.setBounds(281, 185, 124, 25);
		getContentPane().add(btnDeleteProduct);

		nameTextField = new JTextField();
		nameTextField.setBounds(12, 272, 116, 22);
		getContentPane().add(nameTextField);
		nameTextField.setColumns(10);

		surnameTextField = new JTextField();
		surnameTextField.setBounds(138, 272, 116, 22);
		getContentPane().add(surnameTextField);
		surnameTextField.setColumns(10);

		addressTextField = new JTextField();
		addressTextField.setBounds(266, 272, 116, 22);
		getContentPane().add(addressTextField);
		addressTextField.setColumns(10);

		phoneTextField = new JTextField();
		phoneTextField.setBounds(394, 272, 116, 22);
		getContentPane().add(phoneTextField);
		phoneTextField.setColumns(10);

		JLabel lblName = new JLabel("Name");
		lblName.setBounds(12, 252, 56, 16);
		getContentPane().add(lblName);

		JLabel lblSurname = new JLabel("Surname");
		lblSurname.setBounds(138, 252, 56, 16);
		getContentPane().add(lblSurname);

		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(266, 252, 56, 16);
		getContentPane().add(lblAddress);

		JLabel lblPhone = new JLabel("Phone");
		lblPhone.setBounds(394, 252, 56, 16);
		getContentPane().add(lblPhone);

		productNameTextField = new JTextField();
		productNameTextField.setBounds(12, 338, 116, 22);
		getContentPane().add(productNameTextField);
		productNameTextField.setColumns(10);

		productPPUTextField = new JTextField();
		productPPUTextField.setBounds(138, 338, 116, 22);
		getContentPane().add(productPPUTextField);
		productPPUTextField.setColumns(10);

		stockTextField = new JTextField();
		stockTextField.setBounds(266, 338, 116, 22);
		getContentPane().add(stockTextField);
		stockTextField.setColumns(10);

		JLabel lblProductName = new JLabel("Product Name");
		lblProductName.setBounds(12, 317, 114, 16);
		getContentPane().add(lblProductName);

		JLabel lblPricePerUnit = new JLabel("Price Per Unit");
		lblPricePerUnit.setBounds(138, 317, 114, 16);
		getContentPane().add(lblPricePerUnit);

		JLabel lblDescription = new JLabel("Stock");
		lblDescription.setBounds(266, 317, 114, 16);
		getContentPane().add(lblDescription);

		btnAddClient = new JButton("Add Client");
		btnAddClient.setBackground(new Color(204, 255, 204));
		btnAddClient.setBounds(532, 271, 97, 25);
		getContentPane().add(btnAddClient);

		btnUpdateClient = new JButton("Update client");
		btnUpdateClient.setBounds(645, 271, 138, 25);
		getContentPane().add(btnUpdateClient);

		btnDeleteClient = new JButton("Delete Client");
		btnDeleteClient.setBackground(new Color(255, 0, 0));
		btnDeleteClient.setBounds(696, 185, 124, 25);
		getContentPane().add(btnDeleteClient);

		JLabel lblClients = new JLabel("Clients");
		lblClients.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblClients.setBounds(577, 13, 56, 16);
		getContentPane().add(lblClients);

		JLabel lblProducts = new JLabel("Products");
		lblProducts.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblProducts.setBounds(138, 13, 114, 16);
		getContentPane().add(lblProducts);

		JLabel lblProductIdTo = new JLabel("Product ID to select");
		lblProductIdTo.setBounds(12, 189, 116, 16);
		getContentPane().add(lblProductIdTo);

		deleteProductID = new JTextField();
		deleteProductID.setBounds(138, 186, 116, 22);
		getContentPane().add(deleteProductID);
		deleteProductID.setColumns(10);

		JLabel lblCreateOrder = new JLabel("Create Order");
		lblCreateOrder.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblCreateOrder.setBounds(12, 376, 114, 16);
		getContentPane().add(lblCreateOrder);

		JLabel lblProductForm = new JLabel("Client Form");
		lblProductForm.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblProductForm.setBounds(12, 218, 114, 16);
		getContentPane().add(lblProductForm);

		JLabel lblDeleteAProduct = new JLabel("Delete a product");
		lblDeleteAProduct.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblDeleteAProduct.setBounds(8, 160, 175, 16);
		getContentPane().add(lblDeleteAProduct);

		JLabel label = new JLabel("Product Form");
		label.setFont(new Font("Tahoma", Font.PLAIN, 17));
		label.setBounds(8, 298, 114, 16);
		getContentPane().add(label);

		deleteClientId = new JTextField();
		deleteClientId.setBounds(553, 186, 116, 22);
		getContentPane().add(deleteClientId);
		deleteClientId.setColumns(10);

		JLabel lblClientIdTo = new JLabel("Client ID to select");
		lblClientIdTo.setBounds(434, 189, 107, 16);
		getContentPane().add(lblClientIdTo);

		orderQuantity = new JTextField();
		orderQuantity.setBounds(649, 428, 76, 22);
		getContentPane().add(orderQuantity);
		orderQuantity.setColumns(10);

		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(645, 405, 56, 16);
		getContentPane().add(lblQuantity);

		btnAddToOrder = new JButton("Add");
		btnAddToOrder.setBounds(737, 427, 76, 25);

		btnComplete = new JButton("Complete");
		btnComplete.setBounds(553, 478, 97, 25);
		getContentPane().add(btnComplete);

		btnDelete = new JButton("Delete");
		btnDelete.setBounds(553, 516, 97, 25);
		getContentPane().add(btnDelete);
		getContentPane().add(btnAddToOrder);

		orderTextField = new JTextField();
		getContentPane().add(orderTextField);
		JScrollPane scroll = new JScrollPane(orderTextField,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setSize(360, 102);
		scroll.setLocation(22, 465);
		getContentPane().add(scroll);
	}

	/**
	 * Displays pop-up window
	 * 
	 * @param messageText displayed text
	 */
	public void displayMessage(final String messageText) {
		JOptionPane.showMessageDialog(this, messageText);
	}

	/**
	 * Returns product table
	 * @return
	 */
	public JTable getProductTable() {
		return table;
	}

	/**
	 * Returns client table
	 * @return
	 */
	public JTable getClientTable() {
		return table2;
	}

	/**
	 * Returns product id
	 * @return
	 */
	public int getDeleteProductId() {
		return Integer.parseInt(deleteProductID.getText());
	}

	/**
	 * Methods for adding button action listeners
	 * @param actionListener
	 */
	public void addCompleteOrderButtonActionListener(final ActionListener actionListener) {
		btnComplete.addActionListener(actionListener);
	}

	public void addDeleteOrderButtonActionListener(final ActionListener actionListener) {
		btnDelete.addActionListener(actionListener);
	}

	public void addProductButtonActionListener(final ActionListener actionListener) {
		btnAddProduct.addActionListener(actionListener);

	}

	public void addUpdateProductButtonActionListener(final ActionListener actionListener) {
		btnUpdateProduct.addActionListener(actionListener);

	}

	public void addDeleteProductButtonActionListener(final ActionListener actionListener) {
		btnDeleteProduct.addActionListener(actionListener);
	}

	public void addClientButtonActionListener(final ActionListener actionListener) {
		btnAddClient.addActionListener(actionListener);

	}

	public void addUpdateClientButtonActionListener(final ActionListener actionListener) {
		btnUpdateClient.addActionListener(actionListener);

	}

	public void addDeleteClientButtonActionListener(final ActionListener actionListener) {
		btnDeleteClient.addActionListener(actionListener);
	}

	public void addAddToOrderButtonActionListener(final ActionListener actionListener) {
		btnAddToOrder.addActionListener(actionListener);
	}
	
	/**
	 * Updates client combo box
	 */
	public void updateClientComboBox() {
		comboBox.removeAllItems();
		for (Client c : ClientDao.selectAll()) {
			comboBox.addItem(c);
		}
		getContentPane().add(comboBox);
		getContentPane().revalidate();
		getContentPane().repaint();
	}
	
	/**
	 * Updates Product combo box
	 */
	public void updateProductComboBox() {
		comboBox2.removeAllItems();
		for (Product c : ProductDao.selectAll()) {
			comboBox2.addItem(c);
		}
		getContentPane().add(comboBox2);
		getContentPane().revalidate();
		getContentPane().repaint();
	}

	/**
	 * Sets "in cart logger"
	 * 
	 * @param message
	 */
	public void setOrderTextField(final String message) {
		orderTextField.setText(orderTextField.getText() + "\n" + message);
		getContentPane().revalidate();
		getContentPane().repaint();
	}

	/**
	 * Clears logger
	 */
	public void clearOrderTextField() {
		orderTextField.setText(" ");
	}

	/**
	 * Populates a JTable from a list of objects using reflection 
	 * 
	 * @param       <T>
	 * @param table
	 */
	public <T> void showTable(JTable table, List<T> objects) {
		String[] tableHeader2 = new String[Reflection.tableHeader(objects.get(0)).size()];
		int j = 0;
		for (String s : Reflection.tableHeader(objects.get(0))) {
			tableHeader2[j] = s;
			j++;
		}

		table.setModel(
				new DefaultTableModel(
						new Object[][] { { null, null, null, null }, { null, null, null, null },
								{ null, null, null, null }, { null, null, null, null }, { null, null, null, null }, },
						tableHeader2));
		table.getColumnModel().getColumn(0).setPreferredWidth(47);

		DefaultTableModel model = new DefaultTableModel();

		Object[] columnsName = new Object[Reflection.tableHeader(objects.get(0)).size()];
		List<String> columns = Reflection.tableHeader(objects.get(0));

		for (int i = 0; i < columns.size(); i++) {
			columnsName[i] = columns.get(i);
		}

		model.setColumnIdentifiers(columnsName);

		Object[] rowData = new Object[objects.size() + 1];

		for (Object o : objects) {
			List<String> properties = Reflection.retrieveValues(o);
			for (int i = 0; i < properties.size(); i++) {
				rowData[i] = properties.get(i);
			}
			model.addRow(rowData);
		}
		table.setModel(model);
	}

	/**
	 * Gets the client id from text field
	 * 
	 * @return
	 */
	public int getDeleteClientId() {
		return Integer.parseInt(deleteClientId.getText());
	}

	/**
	 * Gets client name from text field
	 * 
	 * @return
	 */
	public String getClientName() {
		return nameTextField.getText();
	}

	/**
	 * Gets client surname from text field
	 * 
	 * @return
	 */
	public String getClientSurname() {
		return surnameTextField.getText();
	}

	/**
	 * Gets client address from text field
	 * 
	 * @return return address as String
	 */
	public String getClientAddress() {
		return addressTextField.getText();
	}

	/**
	 * Gets client phone number from text field
	 * 
	 * @return returns phone number as String
	 */
	public String getClientPhone() {
		return phoneTextField.getText();
	}

	/**
	 * Gets product name from text field
	 * 
	 * @return returns product name as String
	 */
	public String getProductName() {
		return productNameTextField.getText();
	}

	/**
	 * Gets product price from text field
	 * 
	 * @return returns price as integer
	 */
	public int getProductPrice() {
		if(productPPUTextField.getText().equals(""))
			return 0;
		return Integer.parseInt(productPPUTextField.getText());
	}

	/**
	 * Gets product stock from text field
	 * 
	 * @return returns stock as Integer
	 */
	public int getProductStock() {
		if(stockTextField.getText().equals(""))
			return 0;
		return Integer.parseInt(stockTextField.getText());
	}

	/**
	 * Gets selected client from combo box
	 * 
	 * @return returns Client object corresponding to selected client
	 */
	public Client getSelectedClient() {
		return (Client) comboBox.getSelectedItem();
	}

	/**
	 * Gets selected product from combo box
	 * 
	 * @return returns Product object corresponding to selected product
	 */
	public Product getSelectedProduct() {
		return (Product) comboBox2.getSelectedItem();
	}

	/**
	 * Gets quantity of product to order
	 * 
	 * @return returns product quantity as integer
	 */
	public int getOrderQuantity() {
		return Integer.parseInt(orderQuantity.getText());
	}
}

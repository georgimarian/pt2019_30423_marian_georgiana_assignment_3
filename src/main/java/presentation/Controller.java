package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.UIManager;

import businessLayer.BillGenerator;
import businessLayer.ClientBLL;
import businessLayer.OrderBLL;
import businessLayer.OrderHasProductBLL;
import businessLayer.ProductBLL;
import model.Client;
import model.Order;
import model.OrderHasProduct;
import model.Product;

/**
 * Class Handling event for every button on UI
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 *
 */
public class Controller {
	private View view;
	private Order toBill;
	private ClientBLL clientBLL;
	private ProductBLL pBLL;
	private OrderBLL oBLL;
	private OrderHasProductBLL ohpBLL;

	/**
	 * Controller constructor
	 * @param view the view associated with the Controller
	 */
	public Controller(View view) {
		this.view = view;
		this.toBill = null;
		this.clientBLL = new ClientBLL();
		this.pBLL = new ProductBLL();
		this.oBLL = new OrderBLL();
		this.ohpBLL = new OrderHasProductBLL();
	}

	/**
	 * Method that begins Controller's action
	 */
	public void start() {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		view.setLocationRelativeTo(null);
		view.setVisible(true);

		initializeButtonListeners();

	}

	/**
	 * Class handling button Action listeners
	 */
	private void initializeButtonListeners() {
		
		view.addDeleteProductButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int pID = view.getDeleteProductId();
					pBLL.deleteProduct(pID);
					view.showTable(view.getProductTable(), pBLL.selectAll());
					view.getContentPane().revalidate();
					view.getContentPane().repaint();
					view.updateProductComboBox();
				} catch (Exception e1) {
					view.displayMessage("Wrong id!");
				}
			}
		});

		view.addProductButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String pName = view.getProductName();
					int pPrice = view.getProductPrice();
					int pStock = view.getProductStock();
					pBLL.insertProduct(new Product(pName, pStock, pPrice));
					view.showTable(view.getProductTable(), pBLL.selectAll());
					view.getContentPane().revalidate();
					view.getContentPane().repaint();
					view.updateProductComboBox();
				} catch (Exception e1) {
					view.displayMessage("Something went wrong. Cannot add product!");
				}
			}
		});

		view.addUpdateProductButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int pID = view.getDeleteProductId();
					String pName = view.getProductName();
					int pPrice = view.getProductPrice();
					int pStock = view.getProductStock();
					
					Product c = pBLL.selectOne(pID);
					c.setStock(pStock);
					c.setPricePerUnit(pPrice);
					c.setProductName(pName);
					pBLL.updateProduct(c);
					view.showTable(view.getProductTable(), pBLL.selectAll());
					view.getContentPane().revalidate();
					view.getContentPane().repaint();
					view.updateProductComboBox();
				} catch (Exception e1) {
					view.displayMessage("Cannot update non-existent product!");
				}
			}
		});

		view.addDeleteClientButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int cID = view.getDeleteClientId();
					clientBLL.deleteClient(cID);
					view.showTable(view.getClientTable(), clientBLL.selectAll());
					view.updateClientComboBox();
					view.getContentPane().revalidate();
					view.getContentPane().repaint();
				} catch (Exception e1) {
					view.displayMessage("Wrong input!");
				}
			}
		});

		view.addUpdateClientButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int cID = view.getDeleteClientId();
					String cName = view.getClientName();
					String cSurname = view.getClientSurname();
					String cAddr = view.getClientAddress();
					String cPhone = view.getClientPhone();
					Client c = clientBLL.selectOne(cID);
					c.setName(cName);
					c.setSurname(cSurname);
					c.setAddress(cAddr);
					c.setPhone(cPhone);
					clientBLL.updateClient(c);
					view.showTable(view.getClientTable(), clientBLL.selectAll());
					view.updateClientComboBox();
					view.getContentPane().revalidate();
					view.getContentPane().repaint();
				} catch (Exception e1) {
					view.displayMessage("Cannot update non existent client!");
				}
			}
		});

		view.addClientButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String cName = view.getClientName();
					String cSurname = view.getClientSurname();
					String cAddr = view.getClientAddress();
					String cPhone = view.getClientPhone();
					Client c = new Client(cName, cSurname, cAddr, cPhone);
					clientBLL.insertClient(c);
					view.showTable(view.getClientTable(), clientBLL.selectAll());
					view.updateClientComboBox();
					view.getContentPane().revalidate();
					view.getContentPane().repaint();
				} catch (Exception e1) {
					view.displayMessage("Something went wrong! Cannot add client");
				}
			}
		});

		view.addAddToOrderButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Client client = view.getSelectedClient();
					Product product = view.getSelectedProduct();
					int quantity = view.getOrderQuantity();
					if (quantity > product.getStock()) {
						throw new Exception("Under stock!");
					}
					product.setStock(product.getStock() - quantity);
					pBLL.updateProduct(product);
					Order order = new Order(0, client.getClientID());
					if(getToBill() == null) {
						order.setOrderID(oBLL.insertOrder(order));
						setToBill(order);
					}
					OrderHasProduct ohp = new OrderHasProduct(getToBill().getOrderID(), product.getProductID(), quantity,
							quantity * product.getPricePerUnit());
					ohpBLL.insertOrderHasProduct(ohp);
					toBill.setTotal(ohp.getPrice());
					oBLL.updateOrder(toBill);
					view.showTable(view.getProductTable(), pBLL.selectAll());
					view.setOrderTextField(product.toString());
					view.getContentPane().revalidate();
					view.getContentPane().repaint();
					view.displayMessage("Item added successfully!");
				} catch (Exception e1) {
					view.displayMessage(e1.toString());

				}
			}
		});

		view.addCompleteOrderButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (getToBill() != null) {
						BillGenerator.generateBill(getToBill());
						view.showTable(view.getProductTable(), pBLL.selectAll());
						view.getContentPane().revalidate();
						view.getContentPane().repaint();
						view.displayMessage("Order completed successfully!");
						view.clearOrderTextField();
						setToBill(null);

					}
				} catch (Exception e1) {
					view.displayMessage(e1.toString());

				}
			}
		});

		view.addDeleteOrderButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (getToBill() != null) {
						oBLL.deleteOrderAssociation(toBill);
						oBLL.deleteOrder(toBill.getOrderID());
						view.clearOrderTextField();
						view.showTable(view.getProductTable(), pBLL.selectAll());
						view.getContentPane().revalidate();
						view.getContentPane().repaint();
						view.displayMessage("Order deleted successfully! Products added back!");
						setToBill(null);
					}
				} catch (Exception e1) {
					view.displayMessage(e1.toString());

				}
			}
		});

	}

	/**
	 * Gets the order to be billed
	 * @return
	 */
	public Order getToBill() {
		return toBill;
	}

	/**
	 * Sets the order to be billed
	 * @param toBill
	 */
	public void setToBill(Order toBill) {
		this.toBill = toBill;
	}
}

package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Class for handling database connection
 * 
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 *
 */
public class ConnectionFactory {
	private static ConnectionFactory singleInstance = new ConnectionFactory();

	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String DBURL = "jdbc:mysql://localhost/order_management";
	private static final String USER = "root";
	private static final String PASS = "";

	public ConnectionFactory() {
		try {
			Class.forName(DRIVER);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Connection createConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(DBURL, USER, PASS);
		} catch (SQLException e) {
			System.out.println("An error occured while connecting");
		}
		return connection;
	}

	public static Connection getConnection() {
		return singleInstance.createConnection();
	}

	public static void close(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				System.out.println("Error while closing connection");
			}
		}
	}

	public static void close(Statement statement) {
		try {
			if (statement != null) {
				statement.close();
			}

		} catch (Exception e) {
			System.out.println("Error while closing statement");

		}
	}

	public static void close(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				System.out.println("Error while closing result set");

			}
		}
	}

}

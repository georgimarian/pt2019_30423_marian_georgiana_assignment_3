package application;
import presentation.Controller;
import presentation.View;

/**
 * Main application class
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 *
 */
public class Application {

	/**
	 * Main method of the application
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			View view = new View();
			Controller controller = new Controller(view);
			controller.start();
			view.setVisible(true);

		} catch (Exception e) {
			System.out.println("ERROR! Something went wrong!");
			e.printStackTrace();
		}
	}
}

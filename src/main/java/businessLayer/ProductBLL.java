package businessLayer;

import java.util.List;

import dao.ProductDao;
import model.Product;

/**
 * Class handling product business logic
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 *
 */
public class ProductBLL {

	/**
	 * Inserts a product in the table
	 * 
	 * @param p product to be inserted
	 */
	public void insertProduct(Product p) {
		ProductDao.insert(p);
	}

	/**
	 * Updates product in table
	 * @param p product to be updated
	 */
	public void updateProduct(Product p) {
		ProductDao.update(p);
	}
	
	/**
	 * Deletes product from table
	 * @param id product identifier
	 */
	public void deleteProduct(int id) {
		ProductDao.delete(id);
	}
	
	public Product selectOne(int id) {
		return ProductDao.findById(id);
	}

	public List<Product> selectAll() {
		return ProductDao.selectAll();
	}
}

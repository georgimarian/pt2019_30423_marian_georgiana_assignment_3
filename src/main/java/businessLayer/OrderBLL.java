package businessLayer;

import java.util.List;

import dao.OrderDao;
import dao.OrderHasProductDao;
import dao.ProductDao;
import model.Order;
import model.OrderHasProduct;
import model.Product;
/**
 * Class handling order business logic
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 *
 */
public class OrderBLL {

	public int insertOrder(Order p) {
		return OrderDao.insert(p);
	}
	public void updateOrder(Order p) {
		OrderDao.update(p);
	}
	public void deleteOrder(int p) {
		OrderDao.delete(p);
	}
	
	public void deleteOrderAssociation(Order order) {
		OrderHasProductBLL ohpBLL = new OrderHasProductBLL();
		ProductBLL pBLL = new ProductBLL();
		List<OrderHasProduct> ohp = ohpBLL.selectById(order.getOrderID());
		for (OrderHasProduct o : ohp) {
			Product product = pBLL.selectOne(o.getProduct_productID());
			product.setStock(product.getStock() + o.getQuantity());
			pBLL.updateProduct(product);
		}
		ohpBLL.deleteOrderHasProduct(order.getOrderID());
	}
	public void selectOrder(int p) {
		OrderDao.findById(p);
	}
}

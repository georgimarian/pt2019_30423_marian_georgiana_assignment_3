package businessLayer;

import java.util.List;

import dao.OrderHasProductDao;
import model.OrderHasProduct;

/**
 * Class handling orderhasproduct business logic
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 *
 */
public class OrderHasProductBLL {
	public int insertOrderHasProduct(OrderHasProduct p) {
		return OrderHasProductDao.insert(p);
	}
	public void updateOrderHasProduct(OrderHasProduct p) {
		OrderHasProductDao.update(p);
	}
	public void deleteOrderHasProduct(int p) {
		OrderHasProductDao.delete(p);
	}
	
	public List<OrderHasProduct> selectAllOrderHasProduct() {
		return OrderHasProductDao.selectAll();
	}
	
	public List<OrderHasProduct> selectById(int id) {
		return OrderHasProductDao.findById(id);
	}
}

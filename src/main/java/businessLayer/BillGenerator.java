package businessLayer;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import dao.ClientDao;
import dao.OrderHasProductDao;
import model.Order;
import model.OrderHasProduct;

/**
 * Class generating bills for every order 
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 */
public class BillGenerator {
	static int billNumber = 0;
	
	/**
	 * Method generating a text file representing the bill content
	 * @param order The characteristic order object for current order
	 * @return 
	 */
	public static void generateBill(Order order) {
		billNumber++;
		try {
			PrintWriter out = new PrintWriter("bill"+billNumber+".txt");
			out.println("**********************************");
			out.println("BILL SUMMARY");
			out.println("**********************************\n\n");
			out.println(" ");
			out.println("Bill number " + billNumber);
		    out.println(ClientDao.selectOne(order.getClientID()).toString()  + " has bought ");
		    for(OrderHasProduct o : OrderHasProductDao.findById(order.getOrderID())) {
				out.println(o.toString() +"\n");
			}
		    out.println("........................TOTAL    " + order.getTotal() + " lei");
		    out.close();
		    System.out.println("Bill for order " + billNumber + " has been generated");
		}catch(FileNotFoundException e) {
			System.out.println("File not found");
		}
	}
}

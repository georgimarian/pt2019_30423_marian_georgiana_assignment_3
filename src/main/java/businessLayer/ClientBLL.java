package businessLayer;

import java.util.List;

import dao.ClientDao;
import model.Client;

/**
 * Class handling client business logic
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 *
 */
public class ClientBLL {
	public void insertClient(Client p) {
		ClientDao.insert(p);
	}
	
	public void updateClient(Client p) {
		ClientDao.update(p);
	}
	
	public void deleteClient(int p) {
		ClientDao.delete(p);
	}
	
	public Client selectOne(int p) {
		return ClientDao.selectOne(p);
	}
	
	public List<Client> selectAll() {
		return ClientDao.selectAll();
	}
}

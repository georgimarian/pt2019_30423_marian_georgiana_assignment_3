package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import model.Order;
import model.OrderHasProduct;

/**
 * Class performing CRUD operations on OrderHasProduct table
 * 
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 *
 */
public class OrderHasProductDao {
	private static final String insertStatementString = "INSERT INTO order_management.order_has_product (order_orderID, product_productID, quantity, price)"
			+ " VALUES (?,?,?,?)";
	private static final String findStatementString = "SELECT * FROM order_management.order_has_product where order_orderID = ?";

	private static final String deleteStatementString = "DELETE FROM order_management.order_has_product WHERE order_orderID = ?";

	private static final String updateStatementString = "UPDATE order_management.order_has_product SET order_orderID = ?, product_productID = ?, quantity = ?, price = ? WHERE orderID = ?";

	/**
	 * Select list of OrderHasProduct rows by order id
	 * @param orderID
	 * @return
	 */
	public static List<OrderHasProduct> findById(int orderID) {
		List<OrderHasProduct> toReturn = new ArrayList<OrderHasProduct>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {

			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, orderID);
			rs = findStatement.executeQuery();
			while (rs.next()) {

				int order_orderID = rs.getInt("order_orderID");
				int product_productID = rs.getInt("product_productID");
				int qty = rs.getInt("quantity");
				int price = rs.getInt("price");
				toReturn.add(new OrderHasProduct(order_orderID, product_productID, qty, price));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	/**
	 * Select all OrderHasProduct rows
	 * @return
	 */
	public static List<OrderHasProduct> selectAll() {
		List<OrderHasProduct> toReturn = new ArrayList<OrderHasProduct>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {

			findStatement = dbConnection.prepareStatement("SELECT * FROM order_management.order_has_product");
			rs = findStatement.executeQuery();
			while (rs.next()) {
				int order_orderID = rs.getInt("order_orderID");
				int product_productID = rs.getInt("product_productID");
				int qty = rs.getInt("quantity");
				int price = rs.getInt("price");
				toReturn.add(new OrderHasProduct(order_orderID, product_productID, qty, price));
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	/**
	 * Add fields to query statement and insert order in database
	 * @param order
	 * @return
	 */
	public static int insert(OrderHasProduct order) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getOrder_orderID());
			insertStatement.setInt(2, order.getProduct_productID());
			insertStatement.setInt(3, order.getQuantity());
			insertStatement.setInt(4, order.getPrice());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			System.out.println("OrderHasProduct dao insert: " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}

	/**
	 * Add id to query statement and delete order by id in database
	 * @param orderID
	 */
	public static void delete(int orderID) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, orderID);
			deleteStatement.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Order Has Product dao delete: " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}

	/**
	 * Add fields to update statement and update order in database
	 * @param order
	 */
	public static void update(OrderHasProduct order) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setInt(1, order.getOrder_orderID());
			updateStatement.setInt(2, order.getProduct_productID());
			updateStatement.setInt(3, order.getQuantity());
			updateStatement.setInt(4, order.getPrice());
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("OrderHasProduct dao update: " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
}

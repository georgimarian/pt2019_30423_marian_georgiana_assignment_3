package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import model.Order;

/**
 * Class performing CRUD operations on Order table
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 *
 */
public class OrderDao {
	private static final String insertStatementString = "INSERT INTO order_management.order (totalPrice, client_clientID)"
			+ " VALUES (?,?)";
	private static final String findStatementString = "SELECT * FROM order_management.order where orderID = ?";

	private static final String deleteStatementString = "DELETE FROM order_management.order WHERE orderID = ?";

	private static final String updateStatementString = "UPDATE order_management.order SET totalPrice = ?, client_clientID = ? WHERE orderID = ?";

	/**
	 * Select one
	 * @param orderID
	 * @return
	 */
	public static Order findById(int orderID) {
		Order toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {

			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, orderID);
			rs = findStatement.executeQuery();
			rs.next();

			int client_clientID = rs.getInt("client_clientID");
			int total = rs.getInt("totalPrice");
			toReturn = new Order(orderID, total, client_clientID);
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	/**
	 * SELECT ALL
	 */
	public static List<Order> selectAll() {
		List<Order> toReturn = new ArrayList<Order>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {

			findStatement = dbConnection.prepareStatement("SELECT * FROM order_management.order");
			rs = findStatement.executeQuery();
			while (rs.next()) {
				int orderID = rs.getInt("orderID");
				int client_clientID = rs.getInt("client_clientID");
				int total = rs.getInt("totalPrice");
				toReturn.add(new Order(orderID, total,client_clientID));
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	/**
	 * CREATE
	 * 
	 * @param product
	 * @return
	 */
	public static int insert(Order order) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, order.getTotal());
			insertStatement.setInt(2, order.getClientID());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			System.out.println("Order dao insert: " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}

	/**
	 * Delete
	 * 
	 * @param clientID
	 */
	public static void delete(int orderID) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, orderID);
			deleteStatement.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Product dao delete: " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}

	/**
	 * 
	 * @param client
	 */
	public static void update(Order order) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setInt(2, order.getClientID());
			updateStatement.setInt(1, order.getTotal());
			updateStatement.setInt(3, order.getOrderID());
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Order dao update: " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
}

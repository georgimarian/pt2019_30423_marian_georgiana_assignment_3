package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import model.Client;
import model.Product;

/**
 * Class performing CRUD operations on Product table
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 *
 */
public class ProductDao {
	private static final String insertStatementString = "INSERT INTO order_management.product (productName,pricePerUnit,stock)"
			+ " VALUES (?,?,?)";
	private static final String findStatementString = "SELECT * FROM order_management.product where productID = ?";

	private static final String deleteStatementString = "DELETE FROM order_management.product WHERE productID = ?";

	private static final String updateStatementString = "UPDATE order_management.product SET productName = ?, pricePerUnit = ?, stock = ? WHERE productID = ?";

	public static Product findById(int productID) {
		Product toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {

			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, productID);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("productName");
			int stock = rs.getInt("stock");
			int price = rs.getInt("pricePerUnit");
			toReturn = new Product(productID, name, stock, price);
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	/**
	 * SELECT ALL
	 */
	public static List<Product> selectAll() {
		List<Product> toReturn = new ArrayList<Product>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {

			findStatement = dbConnection.prepareStatement("SELECT * FROM order_management.product");
			rs = findStatement.executeQuery();
			while (rs.next()) {
				int productID = rs.getInt("productID");
				String name = rs.getString("productName");
				int stock = rs.getInt("stock");
				int price = rs.getInt("pricePerUnit");
				toReturn.add(new Product(productID, name, stock, price));
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	/**
	 * CREATE
	 * 
	 * @param product
	 * @return
	 */
	public static int insert(Product product) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, 5);
			insertStatement.setString(1, product.getProductName());
			insertStatement.setInt(2, product.getPricePerUnit());
			insertStatement.setInt(3, product.getStock());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			System.out.println("Product dao insert: " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}

	/**
	 * Delete
	 * 
	 * @param clientID
	 */
	public static void delete(int productID) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1, productID);
			deleteStatement.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Product dao delete: " + e.getMessage());
		} finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
	}

	/**
	 * 
	 * @param client
	 */
	public static void update(Product product) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setString(1, product.getProductName());
			updateStatement.setInt(2, product.getPricePerUnit());
			updateStatement.setInt(3, product.getStock());
			updateStatement.setInt(4, product.getProductID());
			updateStatement.executeUpdate();
		} catch (SQLException e) {
			System.out.println("Client dao update: " + e.getMessage());
		} finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
}

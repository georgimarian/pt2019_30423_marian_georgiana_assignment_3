package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import connection.ConnectionFactory;
import model.Client;

/**
 * Class performing database CRUD on client
 * @author Georgiana
 * @version 1.0
 * @since 2019-04-17
 *
 */
public class ClientDao {

	private static final String insertStatementString = "INSERT INTO order_management.client (name,surname,address,phone)"
			+ " VALUES (?,?,?,?)";
	private static final String findStatementString = "SELECT * FROM order_management.client WHERE clientID = ?";
	
	private static final String deleteStatementString = "DELETE FROM order_management.client WHERE clientID = ?";
	
	private static final String updateStatementString = "UPDATE order_management.client SET name = ?, surname = ?, address = ?, phone = ? WHERE clientID = ?";
	
	/**
	 * Select one client from database
	 * @param clientID
	 * @return
	 */
	public static Client selectOne(int clientID) {
		Client toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, clientID);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("name");
			String surname = rs.getString("surname");
			String address = rs.getString("address");
			String phone = rs.getString("phone");
			toReturn = new Client(clientID, name, surname, address, phone);
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	public static List<Client> selectAll() {
		List<Client> toReturn = new ArrayList<Client>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			
			findStatement = dbConnection.prepareStatement("SELECT * FROM order_management.client");
			rs = findStatement.executeQuery();
			while(rs.next()) {
			int clientID = rs.getInt("clientID");
			String name = rs.getString("name");
			String surname = rs.getString("surname");
			String address = rs.getString("address");
			String phone = rs.getString("phone");
			toReturn.add(new Client(clientID, name, surname, address, phone));
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
/**
 * CREATE
 * @param product
 * @return
 */
	public static int insert(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, client.getName());
			insertStatement.setString(2, client.getSurname());
			insertStatement.setString(3, client.getAddress());
			insertStatement.setString(4, client.getPhone());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			System.out.println("Client dao insert: " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
	/**
	 * DELETE
	 * @param clientID
	 */
	public static void delete(int clientID) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setInt(1,clientID);
			deleteStatement.executeUpdate();
			
		}catch(SQLException e) {
			System.out.println("Client dao delete: " + e.getMessage());
		}finally {
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
		}
		
	}
	
	/**
	 * Update Client
	 * @param client
	 */
	public static void update(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		try {
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setString(1, client.getName());
			updateStatement.setString(2, client.getSurname());
			updateStatement.setString(3, client.getAddress());
			updateStatement.setString(4, client.getPhone());
			updateStatement.setLong(5, client.getClientID());
			updateStatement.executeUpdate();
		}catch(SQLException e) {
			System.out.println("Client dao update: " + e.getMessage());
		}finally {
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
}
